const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";
const Vowels = ['a','e','i','o','u','A','E','I','O','U'];

function addHeaderElement(heading) {
    let header = document.createElement("h3");
    header.textContent = heading;
    document.body.appendChild(header);
}

function addKatasElement(info) {
    let newElement = document.createElement("h4");
    newElement.textContent = info;
    newElement.style = ['margin-left:30px'];
    document.body.appendChild(newElement)
}



function kata26() {
    addHeaderElement('kata 26');
    let reversCity = lotrCitiesArray.reverse();
    addKatasElement(JSON.stringify(reversCity));
    return reversCity;
}
function kata27() {
    addHeaderElement('kata 27');
    let sortedCity = lotrCitiesArray.sort();
    addKatasElement(JSON.stringify(sortedCity));
    return sortedCity;
}
function kata28() {
    addHeaderElement('kata 28');
    let sortedCity = lotrCitiesArray.sort(function (a, b) {
        return a.length - b.length;
    });
    addKatasElement(JSON.stringify(sortedCity));
    return sortedCity;
}
function kata29() {
    addHeaderElement('kata 29');
    lotrCitiesArray.pop();
    addKatasElement(JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray;
}
function kata30() {
    addHeaderElement('kata 30');
    lotrCitiesArray.push('Deadest Marshes');
    addKatasElement(JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray;
}
function kata31() {
    addHeaderElement('kata 31');
    lotrCitiesArray.shift();
    addKatasElement(JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray;
}
function kata32() {
    addHeaderElement('kata 32');
    lotrCitiesArray.unshift('Rohan');
    addKatasElement(JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray;
}

function kata25() {
    addHeaderElement('kata 25');
    let cityWithMoreWords = [];
    for (let city of lotrCitiesArray) {
        if(city.indexOf(' ') !== -1) {
            cityWithMoreWords.push(city);
        }
    }
    addKatasElement(JSON.stringify(cityWithMoreWords));
    return cityWithMoreWords;
}

function kata24() {
    addHeaderElement('kata 24');
    let index =  lotrCitiesArray.indexOf('Mirkwood');
    addKatasElement(index);
    return index;
}

function kata23() {
    addHeaderElement('kata 23');
    let isExist = lotrCitiesArray.indexOf('Hollywood') !== -1 ? 'Yes' : 'No';
    addKatasElement(isExist);
    return isExist;
}

function kata22() {
    addHeaderElement('kata 22');
    let isExist = lotrCitiesArray.indexOf('Mirkwood') !== -1 ? 'Yes' : 'No';
    addKatasElement(isExist);
    return isExist;
}

function kata21() {
    addHeaderElement('kata 21');
    let wordStartWithB = [];
    for (let thing of bestThing.split(' ')) {
        if(thing[0] === 'b') {
            wordStartWithB.push(thing);
        }
    }
    addKatasElement(JSON.stringify(wordStartWithB));
    return wordStartWithB;
}

function kata20() {
    addHeaderElement('kata 20');
    let citiesWithLastIndexOr = [];
    for (let city of lotrCitiesArray) {
        if(city.lastIndexOf('or') !== -1) {
            citiesWithLastIndexOr.push(city);
        }
    }
    addKatasElement(JSON.stringify(citiesWithLastIndexOr));
    return citiesWithLastIndexOr;
}

function kata19() {
    addHeaderElement('kata 19');
    let citiesWithDoubbleVowel = [];
    for (let city of gotCitiesCSV.split(',')) {
        for (let vowel of Vowels) {
            if(city.indexOf(vowel+vowel) !== -1) {
                citiesWithDoubbleVowel.push(city);
                break;
            }
        }
    }
    addKatasElement(JSON.stringify(citiesWithDoubbleVowel));
    return citiesWithDoubbleVowel;
}

function kata1() {
    addHeaderElement('kata 1');
    let Cities = gotCitiesCSV.split(',');
    addKatasElement(JSON.stringify(Cities));
    return Cities;
}

function kata2() {
    addHeaderElement('kata 2');
    let words = bestThing.split(' ');
    addKatasElement(JSON.stringify(words));
    return words;
}
function kata3() {
    addHeaderElement('kata 3');
    let citiesWithSemiColons = gotCitiesCSV.split(',').join(';');
    addKatasElement(JSON.stringify(citiesWithSemiColons));
    return citiesWithSemiColons;
}
function kata4() {
    addHeaderElement('kata 4');
    let comaSepCities = lotrCitiesArray.toString();
    addKatasElement(comaSepCities);
    return comaSepCities;
}
function kata5() {
    addHeaderElement('kata 5');
    let firstFiveCities= [];
    let c = 0;
    while(c<5) {
        firstFiveCities.push(lotrCitiesArray[c]);
        c++;
    }
    addKatasElement(JSON.stringify(firstFiveCities));
    return firstFiveCities;
}
function kata6() {
    addHeaderElement('kata 6');
    let lastFiveCities= [];
    let lotcities = lotrCitiesArray.reverse();
    let c = 0;
    while(c<5) {
        lastFiveCities.push(lotcities[c]);
        c++;
    }
    addKatasElement(JSON.stringify(lastFiveCities));
    lotrCitiesArray.reverse();
    return lastFiveCities;
}
function kata7() {
    addHeaderElement('kata 7');
    let threeToFive= [];
    let c = 0;
    while(c<5) {
        if(c>1) {
            threeToFive.push(lotrCitiesArray[c]);
        }
        c++;
    }
    addKatasElement(JSON.stringify(threeToFive));
    return threeToFive;
}
function kata8() {
    addHeaderElement('kata 8');
    let cities = lotrCitiesArray;
    cities.splice(2,1);
    addKatasElement(JSON.stringify(cities));
    return cities;
}
function kata9() {
    addHeaderElement('kata 9');
    let cities = lotrCitiesArray;
    cities.splice(5,2);
    addKatasElement(JSON.stringify(cities));
    return cities;
}
function kata10() {
    addHeaderElement('kata 10');
    let cities = lotrCitiesArray;
    cities.splice(2,1, 'Rohan');
    addKatasElement(JSON.stringify(cities));
    return cities;
}
function kata11() {
    addHeaderElement('kata 11');
    let cities = lotrCitiesArray;
    cities.splice(4,1, 'Deadest Marshes');
    addKatasElement(JSON.stringify(cities));
    return cities;
}
function kata12() {
    addHeaderElement('kata 12');
    let bestThing14 = bestThing.slice(0,14);
    addKatasElement(bestThing14);
    return bestThing14;
}
function kata13() {
    addHeaderElement('kata 13');
    let bestThinglast12 = bestThing.split('').reverse().join('').slice(0,12).split('').reverse().join('');
    addKatasElement(bestThinglast12);
    return bestThinglast12;
}
function kata14() {
    addHeaderElement('kata 14');
    let bestThing23to38 = bestThing.slice(23,38);
    addKatasElement(bestThing23to38);
    return bestThing23to38;
}
function kata15() {
    addHeaderElement('kata 15');
    let bestThinglast12 = bestThing.split('').reverse().join('').substring(0,12).split('').reverse().join('');
    addKatasElement(bestThinglast12);
    return bestThinglast12;
}
function kata16() {
    addHeaderElement('kata 16');
    let bestThing23to38 = bestThing.substring(23,38);
    addKatasElement(bestThing23to38);
    return bestThing23to38;
}
function kata17() {
    addHeaderElement('kata 17');
    let index = bestThing.indexOf('only');
    addKatasElement(index);
    return index;
}
function kata18() {
    addHeaderElement('kata 18');
    let bestThingAsArray = bestThing.split(' ').reverse(),
        index = bestThing.indexOf(bestThingAsArray[0]);
    addKatasElement(index);
    return index;
}


kata1();
kata2();
kata3();
kata4();
kata5();
kata6();
kata7();
kata8();
kata9();
kata10();
kata11();
kata12();
kata13();
kata14();
kata15();
kata16();
kata17();
kata18();
kata19();
kata20();
kata21();
kata22();
kata23();
kata24();
kata25();
kata26();
kata27();
kata28();
kata29();
kata30();
kata31();
kata32();
